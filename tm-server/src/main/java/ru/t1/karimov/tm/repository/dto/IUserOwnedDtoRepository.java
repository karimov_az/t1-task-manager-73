package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends IDtoRepository<M> {

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT e FROM #{#entityName} e WHERE e.userId = :userId ORDER BY :sort")
    List<M> findAllByUserIdAndSort(@NotNull @Param("userId") String userId, @NotNull @Param("sort") String sort);

    @Nullable
    M findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    @Query("SELECT e FROM #{#entityName} e WHERE e.userId = :userId")
    List<M> findByIndexForUser(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    List<M> findByUserId(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @Transactional
    void deleteByUserId(@NotNull String userId);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
