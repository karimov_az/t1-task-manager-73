package ru.t1.karimov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.repository.IProjectRepository;
import ru.t1.karimov.tm.repository.ITaskRepository;

import javax.persistence.Id;
import java.util.Collection;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    private Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskRepository.save(new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        @Nullable final Task task = taskRepository.findById(id).orElse(new Task());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
